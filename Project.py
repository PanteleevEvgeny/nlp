import shelve
import os
import string
import difflib
import re
import sys
import pymorphy2

p = re.compile(r'\W+')
t = re.compile(r'[!,.?;:]')
n = re.compile(r'[!.?]')
morph = pymorphy2.MorphAnalyzer()

def analize(text):
    if text != r'\s':
        d = {}
        bid = {}
        List = p.split(text)
        for word in List:
            if (len(word) > 3):
                if morph.parse(word)[0].normal_form in d:
                    d[morph.parse(word)[0].normal_form] += 1
                else:
                    d[morph.parse(word)[0].normal_form] = 1
        for tens in t.split(text):
            first = ''
            second = ''
            for word in p.split(tens):
                if (len(word) > 3):
                    first = second
                    second = morph.parse(word)[0].normal_form
                    bigramm = first + ' ' + second
                    if (first == r'[А-Яа-я]+') & (second == r'[А-Яа-я]+'):
                        if bigramm in bid:
                            bid[bigramm] += 1
                        else:
                            bid[bigramm] = 1
                else:
                    first = ''
                    second = ''
        for bigramm in bid:
            bid[bigramm] = bid[bigramm]/d[bigramm.split()[0]]
        Len = len(List)
        for word in d:
            d[word] = d[word]/Len
                
        S = 0
        N = 0
        dmin = {}
                
        for word in d:
            S += d[word]
            N += 1
            S = S/N
                
        for word in d:
            if d[word] > S:
                dmin[word] = d[word]
        S = 0
        N = 0
                
        for bigramm in bid:
            S += bid[bigramm]
            N += 1
            S = S/N
                
        for bigramm in bid:
            if bid[bigramm] > S:
                dmin[bigramm] = bid[bigramm]
        dmin['LenTens'] = Len/len(n.split(text))
        return dmin

def learn():
    waitlabel = Label(panelFrame, text = 'Please, wait...')
    waitlabel.place(x = 110, y = 10, width = 240, height = 40)
    
    anserlabel = Label(panelFrame, text = 'Learning...')
    anserlabel.place(x = 360, y = 10, width = 240, height = 40)
    
    for i in os.listdir(os.getcwd() + "\\Books"):
        Author = {}
        for j in os.listdir(os.getcwd() + "\\Books" + "\\" + i):
            text = ''
            f = open(os.getcwd() + "\\Books" + "\\" + i + "\\" + j, "r")
            for line in f:
                text += line
            f.close()
            Author[j] = analize(text)
        dat = shelve.open(os.getcwd() + "\\memory.txt")
        dat[i] = Author
        dat.close()

def text_checking(text):
    Dict = analize(text)
    f = shelve.open(os.getcwd() + "\\memory.txt")
    MAX = 0
    for autor in f:
        print(autor)
        for text in f[autor]:
            S = 0
            mod1 = 0
            mod2 = 0
            for word in Dict:
                if word in f[autor][text]:
                    print(word)
                    S = S + (f[autor][text][word] * Dict[word])
                    mod1 = mod1 + (Dict[word]**2)
                    mod2 = mod2 + (f[autor][text][word]**2)
            S = S/((mod1**(0.5))*(mod2**(0.5)))
            if (MAX < S):
                MAX = S
                Result = autor
    f.close()
    return Result

def go():
    result = ''
    TEXT = textbox.get('1.0', 'end')
    if TEXT != '':
        result = text_checking(TEXT)

        anserlabel = Label(panelFrame, text = result)
        anserlabel.place(x = 360, y = 10, width = 240, height = 40)

        waitlabel = Label(panelFrame, text = 'Ready!')
        waitlabel.place(x = 110, y = 10, width = 240, height = 40)

from tkinter import *
root = Tk()
root.title('NLP')
    
panelFrame = Frame(root, height = 60, bg = 'gray')
textFrame = Frame(root, height = 210)

panelFrame.pack(side = 'top', fill = 'x')
textFrame.pack(side = 'bottom', fill = 'both', expand = 1)

textbox = Text(textFrame, font='Arial 14', wrap='word')
scrollbar = Scrollbar(textFrame)

scrollbar['command'] = textbox.yview
textbox['yscrollcommand'] = scrollbar.set

textbox.pack(side = 'left', fill = 'both', expand = 1)
scrollbar.pack(side = 'right', fill = 'y')

waitlabel = Label(panelFrame, text = 'Hello')
waitlabel.place(x = 110, y = 10, width = 240, height = 40)
anserlabel = Label(panelFrame, text = 'Writer:')
anserlabel.place(x = 360, y = 10, width = 240, height = 40)

gobutton = Button(panelFrame, text = 'GO', command=go)
gobutton.place(x = 10, y = 10, width = 40, height = 40)
learnbutton = Button(panelFrame, text = 'Learn', command=learn)
learnbutton.place(x = 60, y = 10, width = 40, height = 40)

root.mainloop()
            

                            
                            
                        
                           
                
    
                    
            
            
